var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 6000 }
]

let index = 0
let waktu = 10000

function bacaBuku() {
    readBooksPromise(waktu, books[index])
        .then((resolve) => {
            waktu = resolve
            index++;
            if (books[index] != null) {
                bacaBuku()
            } else {
                console.log("sudah selesai")
            }
        })
        .catch((error) => {
            console.log(error);
        });
}
bacaBuku() 