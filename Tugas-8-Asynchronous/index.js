var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 6000 }
]

let index = 0
let waktu = 10000

function bacaBuku() {
    readBooks(waktu, books[index], (callback)=> {
        waktu = callback
        index++;
        if (books[index] != null && (waktu - books[index].timeSpent) >= 0 ) {
            bacaBuku()
        } else {
            console.log("sudah selesai")
        }
    })
}
bacaBuku()