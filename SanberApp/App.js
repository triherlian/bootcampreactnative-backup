
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Tugas12 from './Tugas/Tugas12/App';
import Tugas13 from './Tugas/Tugas13/App';
import Tugas14 from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/index';
import Quiz3 from './Tugas/Quiz3/index';

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Tugas12/> */}
      {/* <Tugas13/> */}
      {/* <Tugas14/> */}
      {/* <Tugas15/> */}
      <Quiz3/>
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
