import React from "react";
import { StyleSheet, Text, View } from "react-native";

import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { CreateAccount, Home, Profile, SignIn, Search, Details, Search2 } from "./Screen";
import { createDrawerNavigator } from "@react-navigation/drawer";

const AuthStack = createStackNavigator()
const Tabs = createBottomTabNavigator()
const HomeStack = createStackNavigator()
const ProfileStack = createStackNavigator()
const SearchStack = createStackNavigator()

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home}></HomeStack.Screen>
    <HomeStack.Screen
      name="Details"
      component={Details}
      options={({ route }) => ({
        title: route.params.name
      })}></HomeStack.Screen>
  </HomeStack.Navigator>
)
const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile}></ProfileStack.Screen>
  </ProfileStack.Navigator>
)
const SearchStackScreen = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search}></SearchStack.Screen>
    <SearchStack.Screen name="Search2" component={Search2}></SearchStack.Screen>
  </SearchStack.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen}></Tabs.Screen>
    <Tabs.Screen name="Search" component={SearchStackScreen}></Tabs.Screen>
  </Tabs.Navigator>
)

const Drawer = createDrawerNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen}></Drawer.Screen>
        <Drawer.Screen name="Profile" component={ProfileStackScreen}></Drawer.Screen>
      </Drawer.Navigator>
      {/* <Tabs.Navigator>
        <Tabs.Screen name="Home" component={HomeStackScreen}></Tabs.Screen>
        <Tabs.Screen name="Search" component={SearchStackScreen}></Tabs.Screen>
      </Tabs.Navigator> */}
      {/* <AuthStack.Navigator>
        <AuthStack.Screen name="SignIn" component={SignIn} options={{ title:'Sign In' }}></AuthStack.Screen>
        <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title:'Create Account' }}></AuthStack.Screen>
      </AuthStack.Navigator> */}
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});