console.log("SOAL NO 1");

function range(startNum = 0, endNum = 0) {
    if (startNum == 0 || endNum == 0) {
        return -1
    }
    var array = []
    if (startNum < endNum) {
        while (startNum <= endNum) {
            array.push(startNum);
            startNum += 1
        }
    } else {
        while (startNum >= endNum) {
            array.push(startNum);
            startNum -= 1
        }
    }
    return array;
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

console.log("");
console.log("");
console.log("");
console.log("SOAL NO 2");
function rangeWithStep(startNum, endNum, step) {
    var array = []
    if (startNum < endNum) {
        while (startNum <= endNum) {
            array.push(startNum);
            startNum += step
        }
    } else {
        while (startNum >= endNum) {
            array.push(startNum);
            startNum -= step
        }
    }
    return array;
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))


console.log("");
console.log("");
console.log("");
console.log("SOAL NO 3");
function sum(startNum, endNum = startNum, step = 1) {
    var array = []
    if (startNum < endNum) {
        while (startNum <= endNum) {
            array.push(startNum);
            startNum += step
        }
    } else {
        while (startNum >= endNum) {
            array.push(startNum);
            startNum -= step
        }
    }

    var sumcount = 0
    for (let a of array) {
        sumcount += a
    }
    return sumcount;
}
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log("");
console.log("");
console.log("");
console.log("SOAL NO 4");
function dataHandling(input) {
    var string = ""
    for (let i1 of input) {
        var index = 0;
        var property = "";
        var linebreak = true;
        for (let i2 of i1) {
            switch (index) {
                case 0:
                    property = "Nomor ID : "
                    linebreak = true
                    break;
                case 1:
                    property = "Nama Lengkap : "
                    linebreak = true
                    break;
                case 2:
                    property = "Nomor ID : "
                    linebreak = false
                    break;
                case 3:
                    property = " "
                    linebreak = true
                    break;
                case 4:
                    property = "Hobi : "
                    linebreak = true
                    break;
            }
            string += property + i2 + (linebreak ? "\n" : "");
            index++;
        }
        string += "\n";
    }
    return string;
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log(dataHandling(input))



console.log("");
console.log("");
console.log("");
console.log("SOAL NO 5");
function balikKata(word) {
    var newWord = ""
    for (let i = word.length; i > 0; i--) {
        newWord += word[i-1]
    }
    return newWord;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("");
console.log("");
console.log("");
console.log("SOAL NO 6");

function convertNumberToNamaBulan(angka){
    var bulan;
    if(angka[0] == "0"){
        angka = angka.replace("0","")
    }
    angka = Number(angka)
    switch (angka) {
        case 1:
            bulan = "Januari";
            break;
        case 2:
            bulan = "Februari";
            break;
        case 3:
            bulan = "Maret";
            break;
        case 4:
            bulan = "April";
            break;
        case 5:
            bulan = "Mei";
            break;
        case 6:
            bulan = "Juni";
            break;
        case 7:
            bulan = "Juli";
            break;
        case 8:
            bulan = "Agustus";
            break;
        case 9:
            bulan = "September";
            break;
        case 10:
            bulan = "Oktober";
            break;
        case 11:
            bulan = "November";
            break;
        case 12:
            bulan = "Desember";
            break;
        default:
            bulan = "Bulan tidak valid"
            break;
    }
    return bulan;
}
function dataHandling2(input){
    var indexNomorId = 0;
    var indexNama = 1;
    var indexProvinsi = 2;
    var indexTTL = 3;
    var indexHobi = 4;

    var newArray = [input[indexNomorId],input[indexNama]+"Elsharawy","Provinsi "+input[indexProvinsi],input[indexTTL],"SMA Internasional Metro"]
    console.log(newArray);
    
    var indexTgl = 0;
    var indexBulan = 1;
    var indexTahun = 2;
    var delimiter = "/"

    var arrayTTL = input[indexTTL].split(delimiter);
    var sortTTL = input[indexTTL].split(delimiter);

    console.log(convertNumberToNamaBulan(arrayTTL[indexBulan]))
    console.log(sortTTL.sort(function (value1, value2) { return value2 - value1 } ))
    console.log(arrayTTL.join("-"))
    console.log(newArray[indexNama].slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);